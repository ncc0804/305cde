const express = require("express");
const app = express();
const path = require("path");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const booksRoutes = require("./api/books");
const tmpControllerRoutes = require("./tmpController");

const cookieParser = require('cookie-parser');
app.use(cookieParser());

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'tmp'));


// Routes which should handle requests
app.use("/books",booksRoutes);
app.use("/onlinelibrary", tmpControllerRoutes);


app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});



app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;