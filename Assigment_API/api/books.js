const express = require('express'); 
const router = express.Router();
const admin = require("firebase-admin");
const serviceAccount = require("../FirebaseKey.json"); //key to connect db

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

var database = admin.firestore(); //connect the db
var docRef = database.collection('books'); //choose the specific collection

var books = function() { // Object model use module and closure
    var bookId, bookName, bookPrice, categorie, author, publisher, publishedDate;

    return {
        NewBook: function(bookId, bookName, bookPrice, categorie, author, publisher, publishedDate) {
            this.bookId = bookId;
            this.bookName = bookName;
            this.bookPrice = bookPrice;
            this.categorie = categorie;
            this.author = author;
            this.publisher = publisher;
            this.publishedDate = publishedDate;
        },
        GetData: function() {
            var StrData = {
                bookId: this.bookId,
                bookName: this.bookName,
                bookPrice: this.bookPrice,
                categorie: this.categorie,
                author: this.author,
                publisher: this.publisher,
               publishedDate: this.publishedDate,
            }
            return StrData;
        }
    };
};

var bookList = {
    list: [],
    getlength: function() {
        return this.list.length;
    },
    addData: function(books) {
        this.list.push(books);
    },
    GetData: function() {
        var list = [];
        this.list.forEach(book => {
            list.push(book.GetData());
        });
        return list;
    }
}



router.get('/', (req, res, next) => { //route to get the target data and print it use json format
    //Get parameter to use in the query 
    var bookName = req.query.bookName;
    console.log(bookName);
    //the detail query send to firebase 
    docRef.where("bookName",">=",bookName).get().then(snapshot => {
            console.log(snapshot.size);
            snapshot.forEach(doc => {
                console.log(doc.data());
                var data = doc.data();
                var book = new books();
                book.NewBook(doc.id, data.bookName, data.bookPrice, data.categorie, data.author, data.publisher, data.publishedDate);
                bookList.addData(book);

            });
            console.log(bookList);
            res.status(200).json({
                response: 'Done ',
                record: bookList.getlength(),
                results: bookList.GetData()
                
            });
            bookList.list = [];

        })
        .catch(err => {
            res.status(201).json({
                response: err.response,
            });
        });
});
router.get('/:bookid', (req, res, next) => {
    const bookid = req.params.bookid;
    if (bookid) {
        docRef.doc(bookid).get().then(doc => {
                if (!doc.exists) {
                    res.status(200).json({
                        status: "success",
                        message: 'No Results ',
                    });
                }
                else {
                    res.status(200).json({
                        status: "success",
                        results: doc.data()
                    });
                }
            })
            .catch(err => {
                res.status(201).json({
                    status: "error",
                    message: err.message,
                });
            });

    }
    else {
        res.status(201).json({
            message: 'Data Not Found'
        });
    }
});

router.post('/', (req, res, next) => { // route to add the new data
    console.log(req.body);
    docRef.add(req.body);

    res.status(200).json({
        response: 'Created book ',
        addData: req.body

    });
});

router.delete('/:bookId', (req, res, next) => {  //route to delete the target data
    const bookId = req.params.bookId;

    docRef.doc(bookId).delete().then(function() {
        res.status(200).json({
            response: 'Book deleted!'
        });

    }).catch(function(error) {
        res.status(201).json({
            response: 'Book not deleted!'
        });

    });
});

router.patch('/:bookId', (req, res, next) => { //route to update the data by bookId
    const BookId = req.params.bookId;
    console.log(BookId);
    var data = req.body;
    console.log(data);
    docRef.doc(BookId).update(data).then(function(data) {
        res.status(200).json({
            response: 'Book updated!'
        });

    }).catch(function(error) {
        res.status(201).json({
            response: 'Book not updated!'
        });
    });



});



module.exports = router;
