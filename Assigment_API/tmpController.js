const express = require('express');
const router = express.Router();
const request = require('request');
const admin = require('firebase-admin');
var nodemailer = require('nodemailer');



var transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 587,
    auth: {
        user: 'ncp0804@gmail.com',
        pass: 'Rr2972866'
    }
});


var db = admin.firestore();
var dbClient = db.collection('client');
var dbLibrary = db.collection('library');


router.get('/home', (req, res, next) => {  //response to handle /home page and check user's login status by condition
    var bookName = req.query.bookName;
    var islogin;
    var uname;
    if (req.cookies.cid === undefined) {
        islogin = false;

    }
    else {  
        islogin = true;
        dbClient.doc(req.cookies.cid).get().then(snapshot => {
            uname = snapshot.data().name;
        });
    }

    setTimeout(function() {  //avoid the data late
        if (bookName) {

            var httpOptions = {
                url: 'https://cde305-ncc0804.c9users.io/books?bookName=' + bookName,
                method: 'GET'
            }
            request.get(httpOptions, function(err, results, body) {
                if (!err && res.statusCode === 200) {
                    res.render('index', {
                        status: true,
                        islogin: islogin,
                        uname: uname,
                        res: JSON.parse(body)
                    });

                }
            });
        }
        else {
            res.render('index', {
                status: false,
                islogin: islogin,
                uname: uname,
                res: "no data"
            });
        }
    }, 1500);

});

router.get('/login', (req, res, next) => { //route to login page

    res.render('login');

});
router.get('/register', (req, res, next) => { //route to registeration page

    res.render('register');

});
router.get('/check', (req, res, next) => { //route to verify the registeration

    res.render('check');

});

router.post('/register', (req, res, next) => { // handle and check the new registration
 
    dbClient.where("email", "==", req.body.email).get().then(snapshot => {
            if (snapshot.size == 0) {
                var verificationCode = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
                req.body.code = verificationCode;
                req.body.ischecked = false;
                dbClient.add(req.body);
                res.status(200).json({
                    status: true
                });
                var mailOptions = {
                    from: 'ncp0804@gmail.com',
                    to: req.body.email,
                    subject: 'Wellcome to register of online library',
                    text: 'Your verification code is : [ ' + verificationCode + ' ]'
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        console.log(info.response);
                    }
                });
            }
            else {
                res.status(200).json({
                    status: false,
                    message: "This email is exist! "
                });
            }

        })
        .catch(err => {
            res.status(201).json({
                status: false,
                message: err.message,
            });
        });


});


router.post('/login', (req, res, next) => { // check and deal the login of user 

    dbClient.where("email", "==", req.body.email).where("password", "==", req.body.pwd).get().then(snapshot => {
            if (snapshot.size > 0) {
                snapshot.forEach(doc => {
                    var data = doc.data();

                    if (data.ischecked) {
                        res.cookie("cid", doc.id, { maxAge: 800000 });
                        res.status(200).json({
                            status: true,
                            message: "Success!"
                        });

                    }
                    else {
                        res.status(200).json({
                            status: false,
                            message: "The account need to verification, the verification code in your email."
                        });

                    }

                });

            }
            else {
                res.status(200).json({
                    status: false,
                    message: "The account or password not success!"
                });
            }

        })
        .catch(err => {
            res.status(201).json({
                status: "error",
                message: err.message,
            });
        });

});
router.post('/check', (req, res, next) => { //check the verification code given from user is correct or not to the registered email.

    dbClient.where("email", "==", req.body.email).get().then(snapshot => {
        if (snapshot.size > 0) {
            snapshot.forEach(doc => {
                var data = doc.data();

                if (data.code == req.body.code) {
                    dbClient.doc(doc.id).update({ ischecked: true }).then(function(data) {
                        res.status(200).json({
                            status: "true",
                            message: 'Success!'
                        });

                    })
                }
                else {
                    res.status(200).json({
                        status: "false",
                        message: 'The code not match!'
                    });


                }
            });
        }
        else {
            res.status(200).json({
                status: "false",
                message: 'The email not success!'
            });

        }
    });



});

router.get('/mylibrary', (req, res, next) => { //a independent library page for user manage their books 
    if (req.cookies.cid === undefined) {
        res.redirect('home');
    }
    var mybookslist = [];
    dbLibrary.where("cid", "==", req.cookies.cid).get().then(snapshot => {
        if (snapshot.size > 0) {
            snapshot.forEach(doc => {
                var clientjson = doc.data();
                clientjson.id = doc.id;
                mybookslist.push(clientjson);

            });
            res.render('mylibrary', {
                status: true,
                res: mybookslist
            });
        }
        else {
            res.render('mylibrary', {
                status: false,
                message: 'empty!'
            });

        }
    });


});
router.post('/mylibrary/addbook', (req, res, next) => { //add the book to user's own library 
       
    var httpOptions = {
        url: 'https://cde305-ncc0804.c9users.io/books/' + req.body.bid,
        method: 'GET'
    }

    request.get(httpOptions, function(err, results, body) {
        if (!err && res.statusCode === 200) {
            var data = JSON.parse(body);
            var bookjson = data.results;
            bookjson.bid = req.body.bid;
            bookjson.cid = req.cookies.cid;


            dbLibrary.where("bid", "==", req.body.bid).where("cid", "==", req.cookies.cid).get().then(snapshot => {
                if (snapshot.size > 0) {
                    res.status(200).json({
                        status: true,
                        message: "This book is exist!"
                    });

                }
                else {
                    dbLibrary.add(bookjson);
                    res.status(200).json({
                        status: true,
                        message: "Success!"
                    });
                }
            });
        }
        else {
            res.status(200).json({
                status: false,
                message: "Not success ."
            });

        }
    });
});

//post method for insert data 
router.post('/mylibrary/updatebook', (req, res, next) => {
    dbLibrary.doc(req.body.bid).update({

        bookName: req.body.bookName

    }).then(function(data) {
        res.status(200).json({
            status: true,
            message: 'update success!'
        });

    }).catch(err => {
        res.status(201).json({
            status: false,
            message: "update not success!",
        });
    });

});

//post method for insert data 
router.post('/mylibrary/deletebook', (req, res, next) => {
    dbLibrary.doc(req.body.bid).delete().then(function() {
        res.status(200).json({
            status: true,
            message: "Delete success!",
        });

    }).catch(err => {
        res.status(201).json({
            status: false,
            message: "Delete not success!",
        });
    });

});

router.get('/logout', (req, res, next) => {  //delete the login cookie
    res.clearCookie("cid");
    res.redirect('home');

});



module.exports = router;
