        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyAefLs1m2k-qonlLJKMKqgRG9U6l_LLq9s",
            authDomain: "challenge3-92ae3.firebaseapp.com",
            databaseURL: "https://challenge3-92ae3.firebaseio.com",
            projectId: "challenge3-92ae3",
            storageBucket: "",
            messagingSenderId: "601139721238"
        };
        firebase.initializeApp(config);

        var productList = [];  

        function getFirbaseData() {  //use promise to check ifirebase that if have data or no connect error,return resolve ,else return reject

            return new Promise(function(resolve, reject) {
                firebase.database().ref('item/').once('value', function(snapshot) {
                    console.log(snapshot.val());
                    if (snapshot) {
                        resolve(snapshot);
                    }
                    else {
                        reject("Cannot cannect firebase OR no data!");
                    }

                });


            });


        }

        function listItem(snapshot) { //Loop the items data and output it with html
            snapshot.forEach(function(data) {

                var id = data.val().id;
                var title = data.val().title;
                var desc = data.val().desc;
                var pic = data.val().pic;
                var cost = data.val().cost;
                var arr = [id, title, pic, desc, cost];
                //alert("db data :" + arr);
                productList.push(arr);


                var html = '<div class="itemInfo" >' +
                    '<div class="title">' + title + '</div>' +
                    '<div><img class="pic" src="' + pic + '"></div>' +
                    '<div><textarea class="desc" readonly >' + desc +
                    '</textarea></div>' +
                    '<div>Item Price : <span class="pirce">' + cost + '</span></div>' +
                    '<div><button class="addBtn" onclick="addToCart(\'' + id + '\')" >Add To Basket</button></div></div>';
                $('#itemView').append(html);


            });
        }
        
        getFirbaseData().then(  //decide the result of list item
            function(data) {
                listItem(data);
            },
            function(data) {
                alert(data);
            }
        );



        // basket object use the module and closures 

        var basket = {
            baskets: [],
            totalCost: function() {
                var totalCost = 0;
               
                for (var i = 0; i < this.baskets.length; i++) { //get the total cost of all items
                    totalCost += this.baskets[i].totalCost();
                }
                
                return totalCost;


            },
            totalQty: function() { //count the total quantity of specified item
                var totalQty = 0;
                for (var i = 0; i < this.baskets.length; i++) {
                    totalQty += this.baskets[i].getQty();
                }
                //alert("The basket totalCost is " + totalQty);
                return totalQty;

            },
            showItemToBasket: function() {  //handle the event that  add selected product to the basket with html
                var item = this.baskets.pop();
                this.baskets.push(item);

                var html = '<div class="basketItem" data-id="' + item.id + '" >' +
                '<button class="basketItemRemove" onclick="basket.removeItem(\'' + item.id + '\',this)">Remove</button>'+
                    '<div class="bItemTitle">' + item.title + '</div>' +
                    '<div><textarea class="desc" readonly >' + item.desc +
                    '</textarea></div>' +
                    '<div class="basketItemInfo">Cost : ' + item.cost + ' | Qty : <input class="basketItemQty" onchange="basket.updateItemQty(\'' + item.id + '\',this)" type="number" min=1 max=100 value="' + item.qty + '" /> | TotalCost : <span class="itemTotalCost">' + item.totalCost() + '</span></div><hr></div>';

                $('#itemList').append(html);
                this.updateOrder();


            },
            addItemToCart: function(item) { //item add to cart is successful
                this.baskets.push(item);
                alert("item added");
            },
            removeItem: function(itemid,obj) {
                  for (var i = 0; i < this.baskets.length; i++) {
                    if (itemid == this.baskets[i].id) {
                        this.baskets.splice(i, 1);
                        $(obj).parent('.basketItem').remove();
                        this.updateOrder();
                        
                    }
                }
                this.showBasketsTotal();
               
            },
            updateItemQty: function(itemid, obj) {
                //alert("test update qty!");
                var qty = obj.value;



                for (var i = 0; i < this.baskets.length; i++) {
                    if (itemid == this.baskets[i].id) {
                        this.baskets[i].setQty(qty);
                        $(obj).parent('.basketItemInfo').children(".itemTotalCost").html(this.baskets[i].totalCost());
                        this.updateOrder();
                        //alert("the qty updated!");

                        return;
                    }
                }

            },
            inBasket: function(itemid) {

                //alert(itemid+"/"+this.baskets.length);

                for (var i = 0; i < this.baskets.length; i++) {
                    if (itemid == this.baskets[i].id) {
                        return 0;
                    }

                }
                return 1;

            },
            showBasketsTotal: function() {
                $('#basketQty').html(this.baskets.length);
              
            },
            updateOrder: function() { //display the cart total quantity and total cost
                $('#basketTotalQty').html(this.totalQty());
                $('#basketTotalCost').html(this.totalCost());

            }

        };





        //click add to basket event
        function addToCart(itemid) {

            //check the item if it in the cart,if yes show message 
            if (basket.inBasket(itemid) == 0) {
                alert("this item exist in the cart!")
                return;
            };

            //if the cart don't have this item , put the item into cart
            var id;
            var title;
            var pic;
            var desc;
            var cost;

            for (var i = 0; i < productList.length; i++) {
                if (productList[i][0] == itemid) {
                    id = productList[i][0];
                    title = productList[i][1];
                    pic = productList[i][2]
                    desc = productList[i][3];
                    cost == productList[i][4];

                    cost = productList[i][4];
                    var arr = [id, title, pic, desc, cost];
                    //alert("find array data :" + arr);
                    break;
                }
            }

            var newBasketItem = new item();
            newBasketItem.newItem(id, title, pic, desc, cost, 1);
            newBasketItem.cost;
            //alert(newBasketItem.cost);

            basket.addItemToCart(newBasketItem);

            //alert(basket.baskets[0].cost);
            basket.showBasketsTotal();
            basket.showItemToBasket();
        

            //alert("end add to cart");
        }


        //item object use the module and closures

        var item = function() { 
            var id;
            var title;
            var desc;
            var cost;
            var pic;
            var qty;

            return { // create a new item 
                newItem: function(id, title, pic, desc, cost, qty) {
                    this.id = id;
                    this.title = title;
                    this.pic = pic;
                    this.desc = desc;
                    this.cost = cost;
                    this.qty = qty;
                },
                totalCost: function() { //get the specfied item's total cost
                    var total = (this.qty * this.cost)
                    return total;
                },
                setQty: function(qty) { //set the quantity of item
                    this.qty = qty;
                },
                getQty: function() { // get the specfied item's quantity
                    return parseInt(this.qty);
                }
            }
        };